# Continuous Intergration - Jenkins and Co  

Cette application doit être buildée avec Node.

Avant chaque étape: npm install
Les étapes:
* _Build_: `npm run build-ci`  
* _Test_: `npm run test-ci`
* _Test sans coverage_: `npm run test-ci-without-coverage` 
* _Sonar_: `sonar-scanner -Dsonar.projectKey=<projectKey> -Dsonar.organization=<organisation>`

Utiliser ce formulaire pour rendre les documents pour chaque exercice: https://forms.gle/qiTpwY5PmfSJKxCi6

## Exercice 1 - Build FreeStyle [Jenkins](http://localhost:8080) 6 points  
      
Configurer un build *Jenkins FreeStyle* pour:

* Builder et archiver les artifacts
* Tester avec et sans coverage, utiliser un paramètre booléen pour determiner quel type de test executer. 
* Analyser sur SonarCloud (bonus) 
  
## Exercice 2 - Build Pipeline [Jenkins](http://localhost:8080) 9 points

Ecrire un *Jenkinsfile* pour:

* Afficher la version de l'application l'on build et la branche buildée (la version est dans le fichier package.json)
* Builder et archiver les artifacts
* Executé tester avec et sans coverage, utiliser un paramètre booléen pour determiner quel test executer et archiver le rapport de coverage si celui a été créer (./coverage/lcov-report)
* Analyser sur SonarCloud (bonus) 
    
## Exercice 3 - Pipeline [GitLab](https://gitlab.com) (Bonus)

Ecrire un *.gitlab-ci.yml* pour:

* Builder et archiver les artifacts
* Mettre en cache le dossier node_modules
* Executer les tests avec coverage et archivé le rapport (./coverage/lcov-report)
* Analyser sur SonarCloud (bonus) 
 
